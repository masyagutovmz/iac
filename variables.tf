variable "winrm_1_username"{
  description = "Login to Hyper-V with username"
  type = string
}

variable "winrm_2_password"{
  description = "Login to Hyper-V with password"
  type = string
}