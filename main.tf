module "hyper-v_vm" {
	source = "./modules/hyper-v/vm"
	hyper-v_ip = "192.168.111.115"
	winrm_username = "${var.winrm_1_username}"
	hostname = "test"
	script_name_create = "update-create"
	memory_size = "536870912" # 512Mb=536870912; 1GB=1073741824; 2Gb=2147483648; 3Gb=3221225472; 4Gb=4294967296
	os_name = "debian_11_20Gb"
	switch_name = "Default Switch"
	winrm_password = "${var.winrm_2_password}"
	path_vhd = "d:\\vhd\\"
	state = "Running" # Running; Off
}

module "hyper-v_gitlab-runner" {
	source = "./modules/hyper-v/gitlab-runner"
	hyper-v_ip = "192.168.111.115"
	winrm_username = "${var.winrm_1_username}"
	hostname = "test-2"
	script_name_create = "update-create"
	memory_size = "1073741824" # 512Mb=536870912; 1GB=1073741824; 2Gb=2147483648; 3Gb=3221225472; 4Gb=4294967296
	os_name = "debian_11_20Gb"
	switch_name = "Default Switch"
	winrm_password = "${var.winrm_2_password}"
	path_vhd = "d:\\vhd\\"
	state = "Running" # Running; Off
}