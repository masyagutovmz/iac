Get-NetConnectionProfile
Enable-PSRemoting -Force

$hostName = $env:COMPUTERNAME
$hostIP=(Get-NetAdapter| Get-NetIPAddress).IPv4Address|Out-String
$srvCert = New-SelfSignedCertificate -DnsName $hostName,$hostIP -CertStoreLocation Cert:\LocalMachine\My
$srvCert

Get-ChildItem wsman:\localhost\Listener

Get-ChildItem wsman:\localhost\Listener\ | Where-Object -Property Keys -like 'Transport=HTTP*' | Remove-Item -Recurse

New-Item -Path WSMan:\localhost\Listener\ -Transport HTTPS -Address * -CertificateThumbPrint $srvCert.Thumbprint -Force

New-NetFirewallRule -Displayname 'WinRM - Powershell remoting HTTPS-In' -Name 'WinRM - Powershell remoting HTTPS-In' -Profile Any -LocalPort 5986 -Protocol TCP

Restart-Service WinRM

WinRM e winrm/config/listener

Export-Certificate -Cert $srvCert -FilePath $env:TEMP\PsRemoting-Cert.cer

Import-Certificate -FilePath $env:TEMP\PsRemoting-Cert.cer -CertStoreLocation Cert:\LocalMachine\root\