#!/bin/bash
VAR1=`cat /etc/issue`
VAR2="Debian GNU/Linux 11 \n \l"
if [ "$VAR1" = "$VAR2" ]; then
	sudo bash -c "cp /etc/hosts /tmp && sudo cat /tmp/hosts | sudo sed 's/debian/$1/g' > /etc/hosts" 
	sudo hostnamectl set-hostname $1
    cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF
	sudo modprobe overlay
	sudo modprobe br_netfilter
	cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
	sudo sysctl --system
	sudo apt-get -y install containerd ufw
	sudo mkdir -p /etc/containerd
	containerd config default | sudo tee /etc/containerd/config.toml
	sudo sed '97 i SystemdCgroup = true' /etc/containerd/config.toml > /tmp/config.toml
	sudo rm /etc/containerd/config.toml
	sudo cp /tmp/config.toml /etc/containerd/
	sudo systemctl restart containerd
	ps –ef | grep containerd
	sudo apt-get -y install curl gnupg
	curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
	cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
	sudo apt-get update
	sudo apt-get install -y kubelet kubeadm kubectl
	echo `ip -4 -o a | cut -d " " -f 2,7 | cut -d "/" -f 1 | grep eth0 | cut -d " " -f 2` master-node > /tmp/ip_address
	sudo bash -c "cat /tmp/ip_address >> /etc/hosts"
	
	sudo ufw allow 22/tcp
	sudo ufw allow 6443/tcp
	sudo ufw allow 2379/tcp
	sudo ufw allow 2380/tcp
	sudo ufw allow 10250/tcp
	sudo ufw allow 10251/tcp
	sudo ufw allow 10252/tcp
	sudo ufw allow 10255/tcp

	sudo systemctl enable ufw
	sudo systemctl start ufw
	sudo ufw enable
	
	sudo swapoff -a
	
	sudo systemctl enable kubelet
	
	mkdir -p $HOME/.kube
	sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
	sudo chown $(id -u):$(id -g) $HOME/.kube/config
	kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
	kubectl get nodes
	kubectl get pods --all-namespaces
fi