#!/bin/bash
GITLAB_TOKEN="GR1348941V6JuX3NbU6bjwJvTP8cr"
VAR1=`cat /etc/issue`
VAR2="Debian GNU/Linux 11 \n \l"
if [ "$VAR1" = "$VAR2" ]; then
	sudo bash -c "cp /etc/hosts /tmp && sudo cat /tmp/hosts | sudo sed 's/debian/$1/g' > /etc/hosts" 
#    sudo bash -c "echo '127.0.1.1 $1' >> /etc/hosts"
	sudo hostnamectl set-hostname $1
	sudo apt-get update
	sudo apt-get -y dist-upgrade
	sudo apt-get install -y curl git ca-certificates gnupg lsb-release
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    sudo bash -c 'echo deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null'
    curl -LJO 'https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb'
    sudo apt-get update
    sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-scan-plugin
    sudo dpkg -i gitlab-runner_amd64.deb
    sudo gitlab-runner register --non-interactive --url 'https://gitlab.com/' --registration-token $GITLAB_TOKEN --executor 'docker' --docker-image alpine:latest --description 'docker-runner' --maintenance-note 'Free-form maintainer notes about this runner' --tag-list 'docker' --run-untagged='true' --locked='false' --access-level='not_protected'
    sudo gitlab-runner register --non-interactive --url 'https://gitlab.com/' --registration-token $GITLAB_TOKEN --executor 'shell' --docker-image alpine:latest --description 'shell-runner' --maintenance-note 'Free-form maintainer notes about this runner' --tag-list 'shell_runner' --run-untagged='true' --locked='false' --access-level='not_protected'
    sudo systemctl restart gitlab-runner
    sudo usermod -aG docker gitlab-runner
	sudo curl -L https://github.com/docker/compose/releases/download/1.25.3/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
fi