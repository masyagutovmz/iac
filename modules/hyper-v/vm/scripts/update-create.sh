#!/bin/bash
VAR1=`cat /etc/issue`
VAR2="Debian GNU/Linux 11 \n \l"
if [ "$VAR1" = "$VAR2" ]; then
	sudo bash -c "cp /etc/hosts /tmp && sudo cat /tmp/hosts | sudo sed 's/debian/$1/g' > /etc/hosts" 
#    sudo bash -c "echo '127.0.1.1 $1' >> /etc/hosts"
	sudo sed '2d' /etc/hosts
	sudo hostnamectl set-hostname $1
	sudo apt-get update
	sudo apt-get -y dist-upgrade
fi