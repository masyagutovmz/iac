variable "path_vhd"{
  description = "Path with vhd file"
  type = string
}
variable "hyper-v_ip"{
  description = "Ip address Hyper-V"
  type = string
}

variable "script_name_create"{
  description = "Name of script run to create"
  type = string
}

#variable "script_name_destroy"{
#  description = "Name of script run to destroy"
#  type = string
#}

variable "winrm_username"{
  description = "Login to Hyper-V with username"
  type = string
}

variable "winrm_password"{
  description = "Login to Hyper-V with password"
  type = string
  sensitive   = true
}

variable "hostname"{
  description = "Hostname virtual machine"
  type = string
}

#variable "username"{
#  description = "Login to virtual machine with username"
#  type = string
#}

variable "state"{
  description = "State of vm"
  type = string
}

variable "memory_size"{
  description = "Memory size virtual machine (bytes)"
  type = string
}

variable "os_name"{
  description = "OS virtual machine"
  type = string
}

variable "switch_name"{
  description = "Switch name of hyper-v"
  type = string
}

#variable "hdd_size"{
#  description = "HDD size of virtual machine (20 Gb)"
#  type = string
#}

variable "cpu_count"{
  description = "Processor count"
  type = string
  default = "1"
}
